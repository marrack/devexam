<?php

namespace App\Model;

class DevExam
{
	const FILENAME_LIMIT = 8;

	public static function convertPathToArray($pathArray)
	{
		$pathConverted = array();

		foreach ($pathArray as $key => $pathArrayElement)
		{
			$path = explode('/', substr($pathArrayElement, 1));
			$pathConverted[$key] = $path;
		}

		return $pathConverted;
	}

	public static function convertToTreeArray($pathConverted)
	{
		$treeArray = array();
		$filter = '.txt';

		foreach($pathConverted as $pathConvertedElement)
		{
			// reference to tree array
			$ref = &$treeArray;

			foreach ($pathConvertedElement as $pathIndex)
			{
				// check if file or directory
				if (preg_match("/{$filter}/i", $pathIndex))
				{
					// give appropriate index and sort
					$index = 0;

					foreach ($ref as $key => $elem)
					{
						if (is_numeric($key))
						{
							$index++;
						}
					}

					$ref[$index] = $pathIndex;
					asort($ref);
				}
				else
				{
					// create new array and put reference to that array
					if (!isset($ref[$pathIndex]))
					{
						$ref[$pathIndex] = array();
					}

					$ref = &$ref[$pathIndex];
				}
			}
		}

		return $treeArray;
	}

	public static function convertArrayToString($pathArray, $branchLimit, $leafLimit)
	{
		$treeArray = $pathArray;
		$ref = &$treeArray;
		$ctr = 0;

		self::createTreeString($ref, $ctr, $branchLimit, $leafLimit);
	}

	public static function generateFileArray($basePath, $pathCount, $depthLimit, $fileLimit)
	{
		// check if pathCount will be enough to satisfy depthLimit and fileLimit
		try
		{
			if ($pathCount > $depthLimit * $fileLimit)
			{
				throw new \Exception("path too much for depth limit and file limit");
			}
		}
		catch (\Exception $e)
		{
			echo 'Message: ' . $e->getMessage();
		}

		// instantiate variables
		$pathArray = array();
		$depthCtr = array_fill(1, $depthLimit, 0);
		$fileCount = rand(1, $fileLimit);

		for ($x = 0; $x < $pathCount; $x++)
		{
			// get depth
			$depth = self::getPathDepth($depthCtr, $fileLimit);

			// generate path
			$fileName = md5(rand());
			$fileName = substr($fileName, 1, self::FILENAME_LIMIT);
			$path = $basePath;

			for ($y = 0; $y <= $depth; $y++)
			{
				if ($y === $depth)
				{
					$path .= $fileName . '.txt';
				}
				else
				{
					$path .= 'folder' . ($y + 1) . '/';
				}
			}

			array_push($pathArray, $path);
		}

		return $pathArray;
	}

	// misc functions

	private function createTreeString(&$ref, &$ctr, $branchLimit, $leafLimit)
	{
		$leafCtr = 0;

		while (count($ref) > $branchLimit)
		{
			array_shift($ref);
		}

		foreach ($ref as $key => $tree)
		{
			$trailingSpaces = '';

			for ($x = 0; $x < $ctr; $x++)
			{
				$trailingSpaces .= '    ';
			}


			if (is_array($tree))
			{
				echo $trailingSpaces . $key . '</br>';
			}
			else
			{
				if ($leafCtr < $leafLimit)
				{
					echo $trailingSpaces . $tree . '</br>';
					$leafCtr++;
				}
			}

			$ref = $tree;
			$branchCtr ++;
		}

		$ctr++;
		self::createTreeString($ref, $ctr, $branchLimit, $leafLimit);
	}

	private function getPathDepth(&$depthCtr, $fileLimit)
	{
		$depth = array_rand($depthCtr);

		if ($depthCtr[$depth] === $fileLimit)
		{
			// remove depth from list of counters then get another one
			unset($depthCtr[$depth]);
			$depth = array_rand($depthCtr);
		}

		$depthCtr[$depth]++;

		return $depth;
	}
}