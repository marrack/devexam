<?php
require_once __DIR__ . '/app/Model/DevExam.php';

$pathArray = \App\Model\DevExam::generateFileArray('/home/user/', 15, 3, 5);
$pathConverted = \App\Model\DevExam::convertPathToArray($pathArray);
$treeArray = \App\Model\DevExam::convertToTreeArray($pathConverted);

echo '<p>Generate Path Array</p><pre>';
var_dump($pathArray);
echo '</pre>';

echo '<p>Convert Path to Array</p><pre>';
var_dump($pathConverted);
echo '</pre>';

echo '<p>Convert Path to Tree Array</p><pre>';
var_dump($treeArray);
echo '</pre>';

echo '<p>Convert Tree Array to String</p><pre>';
\App\Model\DevExam::convertArrayToString($treeArray, 3, 2);
echo '</pre>';