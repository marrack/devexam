<?php

class DevExamTest extends \PHPUnit\Framework\TestCase
{
	private $pathLimit = 7;
	private $depthLimit = 3;
	private $fileLimit = 3;
	private $pathArray;
	private $pathConverted;
	private $treArray;

	public function setUp() : void
	{

		$this->pathArray = App\Model\DevExam::generateFileArray('/home/user/', $this->pathLimit, $this->depthLimit, $this->fileLimit);
		$this->pathConverted = \App\Model\DevExam::convertPathToArray($this->pathArray);
		$this->treeArray = \App\Model\DevExam::convertToTreeArray($this->pathConverted);
		\App\Model\DevExam::convertArrayToString($this->treeArray);
	}

	public function testGenerateFileArray()
	{

		// test if number of paths is correct
		$this->assertEquals($this->pathLimit, count($this->pathArray));

		$isDepthLimitTrue = false;

		foreach ($this->pathArray as $pathArrayElement)
		{
			if($isDepthLimitTrue)
			{
				break;
			}

			$search = 'folder' . $this->depthLimit;

			if (preg_match("/{$search}/i", $pathArrayElement))
			{
				$isDepthLimitTrue = true;
			}
		}

		// test if depth limit is in effect
		$this->assertTrue($isDepthLimitTrue);
	}

	// public function testConvertToTreeArray()
	// {

	// }
}